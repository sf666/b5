module "network" {
  source          = "./network"
  token           = var.jr_yc_token
  cloud_id        = var.cloud_id
  folder_id       = var.folder_id
  zone            = "ru-central1-a"
  }

module "instance-1" {
  name      = "vm1"
  source    = "./module-1"
  token     = var.jr_yc_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-a"
  subnet_id = module.network.subnet_id
  image     = "lemp"
  ssh_key   = var.ssh_key
}

module "instance-2" {
  name      = "vm2"
  source    = "./module-1"
  token     = var.jr_yc_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-a"
  subnet_id = module.network.subnet_id
  image     = "lamp"
  ssh_key   = var.ssh_key
}

module "load_balancer" {
  source          = "./load_balancer"
  token           = var.jr_yc_token
  cloud_id        = var.cloud_id
  folder_id       = var.folder_id
  region          = "ru-central1"
  inst_subnet_id  = module.network.subnet_id
  inst1_address   = module.instance-1.ip_address
  inst2_address   = module.instance-2.ip_address
  }
