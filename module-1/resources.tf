data "yandex_compute_image" "image" {
  family = var.image
}

resource "yandex_compute_instance" "yc_ci" {
  name = "instance-${var.name}"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image.id
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "jr:${file(var.ssh_key)}"
  }
}
