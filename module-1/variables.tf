variable "token" {
  description = "yc token"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "yc cloud_id"
  type        = string
  sensitive   = false
}

variable "folder_id" {
  description = "yc folder_id"
  type        = string
  sensitive   = false
}

variable "name" {
  description = "instance name"
  type        = string
  sensitive   = false
}

variable "zone" {
  description = "yc zone"
  type        = string
  sensitive   = false
}

variable "subnet_id" {
  description = "subnet_id"
  type        = string
  sensitive   = false
}

variable "image" {
  description = "yc compute image family"
  type        = string
  default     = "lemp"
  sensitive   = false
}

variable "ssh_key" {
  description = "ssh public key"
  type        = string
  sensitive   = false
}
