variable "token" {
  description = "yc token"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "yc cloud_id"
  type        = string
  sensitive   = false
}

variable "folder_id" {
  description = "yc folder_id"
  type        = string
  sensitive   = false
}

variable "zone" {
  description = "yc zone"
  type        = string
  sensitive   = false
}
