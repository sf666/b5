variable "token" {
  description = "yc token"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "yc cloud_id"
  type        = string
  sensitive   = false
}

variable "folder_id" {
  description = "yc folder_id"
  type        = string
  sensitive   = false
}

variable "zone" {
  description = "yc zone"
  type        = string
  default     = "ru-central1-a"
  sensitive   = false
}

variable "region" {
  description = "yc zone"
  type        = string
  sensitive   = false
}

variable "inst_subnet_id" {
  description = "instance 1 subnet id"
  type        = string
  sensitive   = false
}

variable "inst1_address" {
  description = "instance 1 ip address"
  type        = string
  sensitive   = false
}

variable "inst2_address" {
  description = "instance 2 ip address"
  type        = string
  sensitive   = false
}
