resource "yandex_lb_target_group" "lemp-lamp" {
  name      = "my-target-group"
  region_id = var.region

  target {
    subnet_id = var.inst_subnet_id
    address   = var.inst1_address
  }

  target {
    subnet_id = var.inst_subnet_id
    address   = var.inst2_address
  }
}

resource "yandex_lb_network_load_balancer" "lemp-lamp" {
  name = "lemp-lamp"

  listener {
    name = "my-listener"
    port = 8080
    target_port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.lemp-lamp.id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}