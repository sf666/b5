terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

resource "yandex_iam_service_account" "jr-sa" {
  folder_id = var.folder_id
  name      = "jr-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "jr-sa-editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.jr-sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "jr-sa-static-key" {
  service_account_id = yandex_iam_service_account.jr-sa.id
  description        = "static access key for object storage"
}

# Создаем хранилище
resource "yandex_storage_bucket" "jr-tf-state" {
  bucket     = "jr-tf-state"
  access_key = yandex_iam_service_account_static_access_key.jr-sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.jr-sa-static-key.secret_key
}